"""Testing of the sensor web server"""
from sensor_dashboard.app import app



def test_index():
    """Ensure that the return value is not empty"""
    test_app = app.test_client()
    response = test_app.get("/index")
    assert response.data.decode() != ""
