
[![markdownlint](https://hs-karlsruhe.gitlab.io/ss2020/blju1014/sensor_dashboard/badges/markdownlint.svg)](https://gitlab.com/hs-karlsruhe/ss2020/blju1014/sensor_dashboard/commits/master)
[![yamllint](https://hs-karlsruhe.gitlab.io/ss2020/blju1014/sensor_dashboard/badges/yamllint.svg)](https://gitlab.com/hs-karlsruhe/ss2020/blju1014/sensor_dashboard/commits/master)
[![pylint](https://hs-karlsruhe.gitlab.io/ss2020/blju1014/sensor_dashboard/badges/pylint.svg)](https://hs-karlsruhe.gitlab.io/ss2020/blju1014/sensor_dashboard/lint/)

## Sensor Dashboard - Project documentation

### Project description: 

This project implements a small webserver which servers a table with random values for temperature and a switch status. The table contains a conditional format, that changes the font colors

<!-- blank line -->

### Prerequisites
- Python 3.8
- Pipenv

<!-- blank line -->

### How to setup project
- Use the following command to clone the repository to your local computer:

    `git clone https://gitlab.com/hs-karlsruhe/ss2020/blju1014/sensor_dashboard`

- Install the pipenv environment
- Make sure that all needed modules are installed

<!-- blank line -->

### How to run the project
To run this project, please navigate to the root directory. Then execute the command ``cd sensor_dashboard/; python app.py``. 
The app starts on your local machine on the port 8080. You can open the dashboard via [http://0.0.0.0:8080/index](http://0.0.0.0:8080/index).

<!-- blank line -->

### Developer Flow:
This Project uses the [Simple Git Flow](https://guides.github.com/introduction/flow/) with GitLab merge requests.

