"""Webserver to serve the temperature dashboard of the Karlsruhe University of Applied Sciences """

import random
from flask import Flask, render_template

app = Flask(__name__)


@app.route('/index')
def index():
    """Serves Index Page"""
    temperature_value = random.randint(-10.0, 80.0)
    switch_value = random.choice(["ON", "OFF"])

    if switch_value == "ON":
        switch_color = "green"
    else:
        switch_color = "red"

    if temperature_value < 0:
        temperature_color = "blue"
    elif temperature_value < 40:
        temperature_color = "black"
    else:
        temperature_color = "red"

    return render_template('index.html.j2', title="Index", show_author=True, temperature_value=temperature_value,
                           temperature_color=temperature_color, switch_value=switch_value, switch_color=switch_color)


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8080)
